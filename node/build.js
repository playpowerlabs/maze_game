var WebpackDevServer = require("webpack-dev-server");
var webpack = require("webpack");
var path = require('path');
var root = path.join(__dirname, '..');
var open = require("open");

var env = process.env.NODE_ENV || 'development';
var port = process.env.PORT || 9000;
env = 'production';
var isProd = (env === 'production');
var entry = [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:' + port + '/',
    './main.ts'
];
var plugins = [new webpack.HotModuleReplacementPlugin(), new webpack.NoErrorsPlugin()];
if (isProd) {
    entry = ['./src/MazeGame/index.ts'];
    plugins = [];
}

var compiler = webpack({
    devtool: isProd ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',
    entry: entry,
    output: {
        filename: "main.js",
        path: root + "/build"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: [/bower_components/, /node_modules/],
                loader: 'babel-loader', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015']
                }
            },
            {
                exclude: [/bower_components/, /node_modules/],
                test: /\.tsx?$/,
                loader: 'ts-loader'
            }
        ]
    },
    plugins: [
        ...plugins,
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(env),
            'console': isProd ? 'cc' : 'console'
        })
    ] 
});

compiler.run(function (err, stats) {
    console.log(err, stats);
});