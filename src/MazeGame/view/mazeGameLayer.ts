// import {res} from '../../resource';
import AppConstants, {clearAppConstants} from '../../AppConstants';
import gameHelper from '../../helper/GameHelper';
import eventHelper from '../../helper/EventHelper';

export default class mazeGame extends cc.Layer {
    sprite: any;
    mazeSzH:number;
    mazeSzW:number;
    totalLines:number;
    mazeNode:any;
    car:any;
    remote:any;
    animTimeMove:number;
    animTimeRotate:number;
    remoteLeft:any;
    remoteRight:any;
    remoteUp:any;
    remoteDown:any;
    keyBoardEvent:any;
    listening:boolean;
    movingButtonDir:any;
    moving:any;
    
    constructor() {
        super();
        this.mazeSzH=5;
        this.mazeSzW=5;
        this.animTimeMove=0.3;
        this.animTimeRotate=0.15;
        this.generateMaze();
    }
    
    generateMaze() : void {
        let MazeGame = window['MazeGame'];
        this.listening=false;
        this.movingButtonDir=null;
        this.moving=null;
        let n=this.mazeSzH*this.mazeSzW-1;
        if (n<0) {
            alert("illegal maze dimensions");
            return;
        }
        let horiz =[]; 
        for (let j= 0; j<this.mazeSzH+1; j++) 
            horiz[j]= [];
        let verti =[]; 
        for (let j= 0; j<this.mazeSzH+1; j++) 
            verti[j]= [];    
        let here = [Math.floor(Math.random()*this.mazeSzH), Math.floor(Math.random()*this.mazeSzW)];
        let path = [here]
        let unvisited = [];
        for (let j = 0; j<this.mazeSzH+2; j++) {
            unvisited[j] = [];
            for (let k= 0; k<this.mazeSzW+1; k++)
                unvisited[j].push(j>0 && j<this.mazeSzH+1 && k>0 && (j != here[0]+1 || k != here[1]+1));
        }
        while (0<n) {
            let potential = [[here[0]+1, here[1]], [here[0],here[1]+1], [here[0]-1, here[1]], [here[0],here[1]-1]];
            let neighbors = [];
            for (let j = 0; j < 4; j++) {
                if (unvisited[potential[j][0]+1][potential[j][1]+1])
                    neighbors.push(potential[j]);
            }
            if (neighbors.length) {
                n = n-1;
                let next= neighbors[Math.floor(Math.random()*neighbors.length)];
                unvisited[next[0]+1][next[1]+1]= false;
                if (next[0] == here[0])
                    horiz[next[0]][(next[1]+here[1]-1)/2]= true;
                else 
                    verti[(next[0]+here[0]-1)/2][next[1]]= true;
                path.push(here = next);
            } 
            else 
                here = path.pop();
        }
        // cc.log("horiz :",horiz);
        // cc.log("verti :",verti);
        
        this.mazeNode = new cc.Node();
        let h=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"wall").height;
        let w=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"wall").width;
        this.mazeNode.setAnchorPoint(0.5,0.5);
        let curX=0,curY=w/2;
        this.mazeNode.line = [];
        this.totalLines=2*this.mazeSzH+1;
        for(let i=0;i<2*this.mazeSzH+1;i++)
            this.mazeNode.line[i]=[];
        for(let i=0;i<2*this.mazeSzH+1;i++ ) {
            curX=0;
            if(i%2==0) {
                // curY+=w/2;
                for(let j=0;j<this.mazeSzW;j++) {
                    this.mazeNode.line[i][j]=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"wall");
                    this.mazeNode.line[i][j].setAnchorPoint(0.5,0.5);
                    this.mazeNode.line[i][j].setRotation(90);
                    curX+=h/2;
                    this.mazeNode.line[i][j].setPosition(curX+w/2,curY);
                    this.mazeNode.addChild(this.mazeNode.line[i][j]);
                    curX+=h/2;
                }
                // curY+=w/2;
            }
            else {
                // cc.log("ver");                                    
                curY+=h/2;
                for(let j=0;j<this.mazeSzW+1;j++) {
                    this.mazeNode.line[i][j]=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"wall");
                    this.mazeNode.line[i][j].setAnchorPoint(0.5,0.5);
                    this.mazeNode.line[i][j].setPosition(curX+w/2,curY);
                    this.mazeNode.addChild(this.mazeNode.line[i][j]);
                    curX+=h;
                }
                curY+=h/2;                
            }
        }
        curY+=w/2;
        curX+=w;
        this.mazeNode.setContentSize(curX,curY);
        let sc1=1,sc2=1;
        if(curX>AppConstants.DEVICE_WIDTH*0.9)
            sc1=AppConstants.DEVICE_WIDTH*0.9/curX;
        if(curY>AppConstants.DEVICE_HEIGHT*0.9)
            sc2=AppConstants.DEVICE_HEIGHT*0.9/curY;
            
        this.mazeNode.scale=Math.min(sc1,sc2);
        this.mazeNode.setPosition(AppConstants.DEVICE_WIDTH/2,AppConstants.DEVICE_HEIGHT/2);
        this.addChild(this.mazeNode);
        
        let bgW=(this.mazeNode.width);
        let bgH=(this.mazeNode.height);
        // cc.log(this.mazeNode.width,this.mazeNode.height)
        
        let bg1offset = 50;
        let back1 = new cc.DrawNode();
        back1.drawRect(cc.p(-bg1offset,-bg1offset),cc.p(bgW+bg1offset,bgH+bg1offset),gameHelper.hexToRgb("#34000000"),0);
        this.mazeNode.addChild(back1,-3);
        
        let bg2offset = 30;
        let back2 = new cc.DrawNode();
        back2.drawRect(cc.p(-bg2offset,-bg2offset),cc.p(bgW+bg2offset,bgH+bg2offset),gameHelper.hexToRgb("#6A472E"),0);
        this.mazeNode.addChild(back2,-2);
        
        let bg3offset = 0;
        let back3 = new cc.DrawNode();
        back3.drawRect(cc.p(-bg3offset,-bg3offset),cc.p(bgW+bg3offset,bgH+bg3offset),gameHelper.hexToRgb("#745739"),0);
        this.mazeNode.addChild(back3,-1);
        
        let entrance = new cc.DrawNode();
        let entranceY1=this.mazeNode.line[this.totalLines-2][0].y-h/2;
        let entranceY2=this.mazeNode.line[this.totalLines-2][0].y+h/2;
        entrance.drawRect(cc.p(-bg2offset,entranceY1),cc.p(-bg3offset,entranceY2-w/2),gameHelper.hexToRgb("#745739"),0);
        this.mazeNode.addChild(entrance,-1);
        
        let exit = new cc.DrawNode();
        let exitX1=this.mazeNode.line[1][this.mazeSzW].x+w/2;
        let exitX2=this.mazeNode.width+bg2offset;
        let exitY1=this.mazeNode.line[1][this.mazeSzW-1].y-h/2;
        let exitY2=this.mazeNode.line[1][this.mazeSzW-1].y+h/2;
        entrance.drawRect(cc.p(exitX1,exitY1+w/2),cc.p(exitX2,exitY2),gameHelper.hexToRgb("#745739"),0);
        this.mazeNode.addChild(exit,-1);
        
        this.mazeNode.line[1][this.mazeSzW].visible=false;
        this.mazeNode.line[2*this.mazeSzH-1][0].visible=false;
        
        
      
        let horizMap=[],vertiMap=[];
        for(let i=0;i<horiz.length-2;i++)
            horizMap[i]=2*i+2;
        for(let i=0;i<verti.length-1;i++)
            vertiMap[i]=2*i+1;
                        
        for(let i=0;i<horizMap.length/2;i++) {
            let tmp=horizMap[i];
            horizMap[i]=horizMap[horizMap.length-1-i]
            horizMap[horizMap.length-1-i]=tmp;
        }
        for(let i=0;i<vertiMap.length/2;i++) {
            let tmp=vertiMap[i];
            vertiMap[i]=vertiMap[vertiMap.length-1-i]
            vertiMap[vertiMap.length-1-i]=tmp;
        }
        // cc.log(horizMap,vertiMap);
            
        for(let i=0;i<horiz.length-1;i++) {
            for(let j=0;j<this.mazeSzW-1;j++) {
                if(horiz[i][j])
                    this.mazeNode.line[vertiMap[i]][j+1].visible=false;
            }
        }
        for(let i=0;i<verti.length-2;i++) {
            for(let j=0;j<this.mazeSzW;j++) {
                if(verti[i][j])
                    this.mazeNode.line[horizMap[i]][j].visible=false;
            }
        }
        // return {x: this.mazeSzH, y: this.mazeSzW, horiz: horiz, verti: verti};
        
        for(let i=0;i<2*this.mazeSzH+1;i++ ) {
            if(i%2==0) {
                for(let j=0;j<this.mazeSzW;j++) {
                    if(!this.mazeNode.line[i][j].visible)
                       continue;
                     
                       //left end point
                    if((i-1>0 && this.mazeNode.line[i-1][j].visible) || (i+1<this.totalLines && this.mazeNode.line[i+1][j].visible) || (j==0 || !this.mazeNode.line[i][j-1].visible)) {
                       let box = flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"lineBox");
                       box.setAnchorPoint(0.5,0.5);
                       box.setPosition(this.mazeNode.line[i][j].x-h/2,this.mazeNode.line[i][j].y);
                       this.mazeNode.addChild(box);
                    }
                        //right end point                       
                    if((i-1>0 && j+1<this.mazeSzW+1 && this.mazeNode.line[i-1][j+1].visible) || (i+1<this.totalLines && j+1<this.mazeSzW+1 && this.mazeNode.line[i+1][j+1].visible) || (j+1==this.mazeSzW || !this.mazeNode.line[i][j+1].visible)) {
                       let box = flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"lineBox");
                       box.setAnchorPoint(0.5,0.5);
                       box.setPosition(this.mazeNode.line[i][j].x+h/2,this.mazeNode.line[i][j].y);
                       this.mazeNode.addChild(box);
                    }
                }
            }
            else {
                for(let j=0;j<this.mazeSzW+1;j++) {
                    if(!this.mazeNode.line[i][j].visible)
                       continue;
                       
                        //bottom end point
                    if((i-1>0 && j-1>0 && this.mazeNode.line[i-1][j-1].visible) || (i-1>0 && j<this.mazeSzW && this.mazeNode.line[i-1][j].visible) || (i-2<0 || !this.mazeNode.line[i-2][j].visible)) {
                       let box = flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"lineBox");
                       box.setAnchorPoint(0.5,0.5);
                       box.setPosition(this.mazeNode.line[i][j].x,this.mazeNode.line[i][j].y-h/2);
                       this.mazeNode.addChild(box);
                    }
                        //top end point                       
                    if((i+1<this.totalLines && j-1>0 && this.mazeNode.line[i+1][j-1].visible) || (i+1<this.totalLines && j<this.mazeSzW && this.mazeNode.line[i+1][j].visible) || (i+2>this.totalLines || !this.mazeNode.line[i+2][j].visible)) {
                       let box = flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"lineBox");
                       box.setAnchorPoint(0.5,0.5);
                       box.setPosition(this.mazeNode.line[i][j].x,this.mazeNode.line[i][j].y+h/2);
                       this.mazeNode.addChild(box);
                    }
                }                
            }
        }
        
        this.remote = flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"remote");
        // let remoteOffsetY = AppConstants.DEVICE_HEIGHT*0.1;
        let remoteOffset = 30;
        this.remote.setAnchorPoint(1,0);
        this.remote.setPosition(AppConstants.DEVICE_WIDTH-remoteOffset,remoteOffset);
        this.addChild(this.remote);
        
        this.addListeners();
        
        this.startGame();
        
    }
    startGame():void {
        this.addCarAtBegining();     
          
    }
    addCarAtBegining():void {
        let MazeGame = window['MazeGame'];
        let refH=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"wall").height;
        let refW=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"wall").width;
        
        this.car=flax.assetsManager.createDisplay(MazeGame.res.maze_plist,"car");
        this.car.setScale(0.85);
        let x=this.mazeNode.line[this.totalLines-2][0].x-refW/2-refH/2;
        let y=this.mazeNode.line[this.totalLines-2][0].y;
        this.car.setAnchorPoint(0.5,0.5);
        this.car.setPosition(x,y);
        this.mazeNode.addChild(this.car);

        //initial position
        this.car.cellX=-1;
        this.car.cellY=this.mazeSzH-1;
        this.car.rotation=90;
    }
    
    remoteButtonPressed(event, touch, state)
    {
        // cc.log("mouse"+state);        
        var target = event.getCurrentTarget();
        if(state==eventHelper.ON_BEGAN) {
            target.gotoAndStop(1);
            if(target==this.remote.right)
                this.movingButtonDir="right";
            else if(target==this.remote.left)
                this.movingButtonDir="left";            
            else if(target==this.remote.up)
                this.movingButtonDir="up";
            else if(target==this.remote.down)
                this.movingButtonDir="down";
            this.getCarMoving();
        }
        else if(state==eventHelper.ON_END) {
            target.gotoAndStop(0);
            this.movingButtonDir=null;
        }
        else if(state==eventHelper.ON_OUT) {
            target.gotoAndStop(0);
            this.movingButtonDir=null;
        }
    }
    keyButtonPressed (event, keyStroke, state) {
        // cc.log(state);
        if(state === eventHelper.ON_BEGAN){
            if (keyStroke == cc.KEY.left) 
                this.movingButtonDir="left";            
            if (keyStroke == cc.KEY.right)
                this.movingButtonDir="right";
            if (keyStroke == cc.KEY.up) 
                this.movingButtonDir="up";
            if (keyStroke == cc.KEY.down) 
                this.movingButtonDir="down";
            this.getCarMoving();
                
        }
        if(state === eventHelper.ON_END){
            this.movingButtonDir=null;
        }        
    }
    getCarMoving() {
        if(!this.moving) {
            switch(this.movingButtonDir) {
                case "left" : 
                    this.carGoLeft();
                    break;
                case "right" :
                    this.carGoRight();
                    break;
                case "up" : 
                    this.carGoUp();
                    break;
                case "down" :
                    this.carGoDown();
                    break;
            }
        }
    }
    carGoLeft() {
        if(!this.moving) {
            let h = this.mazeNode.line[0][0].height;
            if(!this.rotateCar("left")) {
                if(this.car.cellX-1>=0 && !this.mazeNode.line[(this.car.cellY)*2+1][(this.car.cellX)].visible) {
                    this.car.cellX--;
                    let x=this.mazeNode.line[1][this.car.cellX].x+h/2;
                    // this.removeListeners();
                    this.moving="left";
                    let arr=[];
                    arr[0]=cc.targetedAction(this.car,cc.moveTo(this.animTimeMove,x,this.car.y));
                    arr[1]=cc.callFunc(function() {
                        // this.addListeners();
                        this.moving=null;
                        this.getCarMoving();
                    }.bind(this));
                    this.runAction(cc.sequence(arr));
                }
            }
        }
    }
    carGoRight() {
        if(!this.moving) {
            let h = this.mazeNode.line[0][0].height;
            if(!this.rotateCar("right")) {            
                let f=0;
                if(this.car.cellX==this.mazeSzW-1 && this.car.cellY==0)
                    f=1;
                if(f || (this.car.cellX+1<this.mazeSzW && !this.mazeNode.line[(this.car.cellY)*2+1][(this.car.cellX)+1].visible)) {
                    this.car.cellX++;
                    let x=this.mazeNode.line[1][this.car.cellX].x+h/2;
                    this.moving="right";
                    let arr=[];
                    if(f)
                        this.movingButtonDir=null;
                    arr[0]=cc.targetedAction(this.car,cc.moveTo(this.animTimeMove,x,this.car.y));
                    arr[1]=cc.callFunc(function() {
                        this.moving=null;
                        if(f) {
                            let act=[];
                            act[0]=cc.delayTime(this.animTimeMove);
                            act[1]=cc.callFunc(function() {
                                this.incrementMazeSize();
                                this.removeAllChildren();
                                this.removeListeners()
                                cc.log("loading next maze")
                                this.generateMaze();
                            }.bind(this));
                            this.runAction(cc.sequence(act));
                        }
                        this.getCarMoving();
                        // this.addListeners();
                    }.bind(this));
                    this.runAction(cc.sequence(arr));                
                }
            }
        }
    }
    carGoUp() {
        if(!this.moving) {
            if(!this.rotateCar("up")) {
                if(this.car.cellY+1<this.mazeSzH && !this.mazeNode.line[2*(this.car.cellY+1)][this.car.cellX].visible) {
                    this.car.cellY++;
                    let y=this.mazeNode.line[1+2*this.car.cellY][0].y;
                    // this.removeListeners();
                    this.moving="up";
                    let arr=[];
                    arr[0]=cc.targetedAction(this.car,cc.moveTo(this.animTimeMove,this.car.x,y));
                    arr[1]=cc.callFunc(function() {
                        // this.addListeners();
                        this.moving=null;
                        this.getCarMoving();
                    }.bind(this));
                    this.runAction(cc.sequence(arr));  
                }
            }
        }
    }
    carGoDown() {
        if(!this.moving) {
            if(!this.rotateCar("down")) {
                if(this.car.cellY-1>=0 && !this.mazeNode.line[2*this.car.cellY][this.car.cellX].visible) {
                    this.car.cellY--;
                    let y=this.mazeNode.line[1+2*this.car.cellY][0].y;
                    // this.removeListeners();
                    this.moving="down";
                    let arr=[];
                    arr[0]=cc.targetedAction(this.car,cc.moveTo(this.animTimeMove,this.car.x,y));
                    arr[1]=cc.callFunc(function() {
                        // this.addListeners();
                        this.moving=null;
                        this.getCarMoving();
                    }.bind(this));
                    this.runAction(cc.sequence(arr));                     
                }
            }
        }
    }
    rotateCar(val) {
        let angle=0;
        let f=0;
        if(val=="left") {
            if((this.car.rotation+360)%360==270) 
                return false;
            this.moving="left";
            angle=270;
        }
        else if(val=="right") {
            if((this.car.rotation+360)%360==90) 
                return false;
            this.moving="right";
            angle=90;
        }
        else if(val=="up") {
            if((this.car.rotation+360)%360==0) 
                return false;
            this.moving="up";
            angle=0;
        }
        else if(val=="down") {
            if((this.car.rotation+360)%360==180) 
                return false;
            this.moving="down";
            angle=180;
        }
        let arr=[];
        arr[0]=cc.targetedAction(this.car,cc.rotateTo(this.animTimeRotate,angle));
        arr[1]=cc.callFunc(function() {
            this.car.rotation=angle;
            cc.log(this.car.rotation);
            this.moving=null;
            this.getCarMoving();
        }.bind(this));
        this.runAction(cc.sequence(arr));    
        return true; 
    }
    removeListeners () {
        if(this.listening) {
            this.listening=false;
            eventHelper.removeEventListener(this.remoteLeft)
            eventHelper.removeEventListener(this.remoteRight)
            eventHelper.removeEventListener(this.remoteUp)
            eventHelper.removeEventListener(this.remoteDown)
            eventHelper.removeEventListener(this.keyBoardEvent)
        }
    }
    addListeners () {
        if(!this.listening) {
            this.listening=true;
            this.remoteLeft = eventHelper.addMouseTouchEvent(this.remoteButtonPressed.bind(this),this.remote.left);
            this.remoteRight = eventHelper.addMouseTouchEvent(this.remoteButtonPressed.bind(this),this.remote.right);
            this.remoteUp = eventHelper.addMouseTouchEvent(this.remoteButtonPressed.bind(this),this.remote.up);
            this.remoteDown = eventHelper.addMouseTouchEvent(this.remoteButtonPressed.bind(this),this.remote.down);
            this.keyBoardEvent = eventHelper.addKeyBoardEvent(this.keyButtonPressed.bind(this),1);
        }
    }
    incrementMazeSize() {
        this.mazeSzH+=2;
        this.mazeSzW+=2;        
    }
    // display(m:any) : void {
        
    //     let text= [];
    //     for (let j= 0; j<m.x*2+1; j++) 
    //     {
    //         let line= [];
    //         if (0 == j%2) {
    //             for (let k=0; k<m.y*4+1; k++)   {
    //                 if (0 == k%4) 
    //                     line[k]= '+';
    //                 else {
    //                     if (j>0 && m.verti[j/2-1][Math.floor(k/4)]) {
    //                         line[k]= ' ';
    //                     }
    //                     else {
    //                         line[k]= '-';
    //                     }
    //                 }
    //             }
    //         }
    //         else {
    //             for (let k=0; k<m.y*4+1; k++) {
    //                 if (0 == k%4) {
    //                     if (k>0 && m.horiz[(j-1)/2][k/4-1])
    //                         line[k]= ' ';
    //                     else
    //                         line[k]= '|';
    //                 }
    //                 else
    //                     line[k]= ' ';
    //             }
    //         }
    //         if (0 == j) 
    //             line[1]= line[2]= line[3]= ' ';
    //         if (m.x*2-1 == j) 
    //             line[4*m.y]= ' ';
    //         text.push(line.join('')+'\r\n');
    //     }
    //     cc.log(text.join(''));
    // }
}