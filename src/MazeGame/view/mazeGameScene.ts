import mazeGameLayer from './mazeGameLayer';
import BgLayer from './BgLayer';
export default class mazeGameScene extends cc.Scene {
    mazeGameLayer: cc.Layer = null;
    bgLayer: cc.Layer = null;
    constructor() {
        super();
        console.log('Inside mazeGameScene');
        this.bgLayer = new BgLayer();
        this.addChild(this.bgLayer);
        this.startGame();
    }

    startGame() {
        console.log('Start the game');
        this.mazeGameLayer = new mazeGameLayer();
        this.addChild(this.mazeGameLayer);
    }
}