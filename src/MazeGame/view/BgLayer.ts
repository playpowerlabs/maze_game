import GameHelper from '../../helper/GameHelper';
import {res} from '../../resource';

export default class BgLayer extends cc.Layer {
    backLayer: any;
    bgColor: any;
    constructor() {
        super();
        this.init();
    }
    
    init() {
        super.init();
        this.bgColor = new cc.LayerColor(GameHelper.hexToRgb("#006633"));
        this.addChild(this.bgColor);
        return true;
    }
}