import GameScene from './view/mazeGameScene';
import helper from '../helper';
import * as _ from 'lodash';

export function init() {
    window['MazeGame'] = window['MazeGame'] || {};
    let AppConstants = window['AppConstants'];
    let MazeGame = window['MazeGame'] || {};


    MazeGame.init = function () {
        MazeGame.data = MazeGame.data || {};
        MazeGame.path = MazeGame.path || '';
    };

    MazeGame.setPath = function (path) {
        MazeGame.path = path;
    };

    MazeGame.getPath = function (asset) {
        if (typeof asset === "undefined")
            return MazeGame.path;
        else
            return MazeGame.path + asset;
    };

    MazeGame.setData = function (data) {
        MazeGame.data = data;
    };

    MazeGame.preload = function () {
        cc.log("inside MazeGame start");
        MazeGame.init();
        MazeGame.loadedRes();
        //adding the language json inside the g_resource
        MazeGame.loadEnglishLanguage();
    };

    MazeGame.loadedRes = function () {
        MazeGame.res = {
            // Jelly_plist: 'res/' + AppConstants.RESOURCE_FOLDER + '/FinalTankWatch.plist',
            // Jelly_png: 'res/' + AppConstants.RESOURCE_FOLDER + '/FinalTankWatch.png',
            maze_png:  "res/maze.png",
            maze_plist:"res/maze.plist"
        };

        MazeGame.res_locale = {
            en: MazeGame.path + "res/locale/en.json",
            zh: MazeGame.path + "res/locale/zh.json"
        };

        MazeGame.g_resources = [];
        for (var i in MazeGame.res) {
            MazeGame.res[i] = MazeGame.path + MazeGame.res[i];
            MazeGame.g_resources.push(MazeGame.res[i]);
        }
    };

    MazeGame.loadEnglishLanguage = function () {
        cc.loader.loadJson(MazeGame.res_locale.en, function (error, data) {
            cc.log('Loaded english');
            MazeGame.locale = data;
            var systemLanguage = AppConstants.APP_LANGUAGE;
            if (systemLanguage !== 'en' && MazeGame.res_locale[systemLanguage]) {
                cc.loader.loadJson(MazeGame.res_locale[systemLanguage], MazeGame.loadedSystemLanguage.bind(this));
            } else {
                //adding the shared resource
                MazeGame.loadAssets();
            }
        }.bind(this));
    };

    MazeGame.loadedSystemLanguage = function (error, data) {
        //merging local language
        MazeGame.locale = _.assign(MazeGame.locale, data);
        MazeGame.loadAssets();
    };

    MazeGame.loadAssets = function () {
        //loading the scripts and resources
        cc.loader.load(MazeGame.g_resources, function () {
            console.log('loaded Assets');
            //TODO Create model
            MazeGame.loaded();
            // cc.log(MazeGame.ApplicationFacade.NAME);
            // MazeGame.ApplicationFacade.getInstance(MazeGame.ApplicationFacade.NAME).startup();
            // var levelProxy = MazeGame.ApplicationFacade.getInstance(MazeGame.ApplicationFacade.NAME).retrieveProxy(MazeGame.model.proxy.LevelProxy.NAME);
            // levelProxy.loadLevel(MazeGame.data, MazeGame.loaded.bind(this));
        });
    };

    MazeGame.loaded = function () {
        helper.PlatformBridge.loaded(MazeGame.data);
    };
    MazeGame.start = function () {
        cc.director.runScene(new GameScene());
    };
}

init();